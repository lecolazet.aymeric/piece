<?php

namespace App\Controller;

use App\Entity\Piece;
use App\Entity\Property;
use App\Form\PieceType;
use App\Repository\PieceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(PieceRepository $pieceRepository): Response
    {
        $pieces = $pieceRepository->findPieceByOrder();

        return $this->render('home/index.html.twig', [
            'pieces' => $pieces,
        ]);
    }

    /**
     * @Route("/piece/create", name="create_piece")
     */
    public function new(Request $request, EntityManagerInterface $em): Response
    {
        $piece = new Piece();
        $form = $this->createForm(PieceType::class,$piece);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($piece);
            $em->flush();

            $this->redirectToRoute('app_home');
        }

        return $this->render('home/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/piece/{id}", name="show_piece")
     */
    public function show(PieceRepository $pieceRepository, $id): Response
    {
        $piece = $pieceRepository->findOneBy(["id" => $id]);

        return $this->render('home/show.html.twig', [
            'piece' => $piece,
        ]);
    }

    /**
     * @Route("/admin/piece/manage", name="manage_piece")
     */
    public function manage(PieceRepository $pieceRepository): Response
    {
        $pieces = $pieceRepository->findPieceByOrder();

        return $this->render('home/manage.html.twig', [
            'pieces' => $pieces,
        ]);
    }

    /**
     * @Route("/admin/piece/edit/{id}", name="piece_edit", methods="GET|POST")
     */
    public function edit($id, Request $request, PieceRepository $pieceRepository, EntityManagerInterface $em)
    {
        $piece = $pieceRepository->find($id);
        $form = $this->createForm(PieceType::class, $piece);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($piece);
            $em->flush();
            return $this->redirectToRoute('manage_piece');
        }

        return $this->render('home/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/piece/delete/{id}", name="piece_delete")
     */
    public function delete(Piece $piece, Request $request, EntityManagerInterface $em): RedirectResponse
    {
        if ($this->isCsrfTokenValid('delete' . $piece->getId(), $request->get('_token'))) {
            $em->remove($piece);
            $em->flush();

        }
        return $this->redirectToRoute('manage_piece');
    }
}

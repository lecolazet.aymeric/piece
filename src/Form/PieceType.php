<?php

namespace App\Form;

use App\Entity\Piece;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PieceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('titre', TextType::class, [
                'label' => 'Titre',
                'required' => false,
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'required' => false,
            ])
            ->add('pays', TextType::class, [
                'label' => 'Pays',
                'required' => false,
            ])
            ->add('date', DateType::class, [
                'label' => 'Année',
                'required' => false,
                'widget' => 'single_text',
            ])
            ->add('tirage', IntegerType::class, [
                'label' => 'Nombre de tirage',
                'required' => false,
                'attr' => [
                    'min' => 0,
                ],
            ])
            ->add('BE', IntegerType::class, [
                'label' => 'BE',
                'required' => false,
                'attr' => [
                    'min' => 0,
                ],
            ])
            ->add('BU', IntegerType::class, [
                'label' => 'BU',
                'required' => false,
                'attr' => [
                    'min' => 0,
                ],
            ])
            ->add('UNC', IntegerType::class, [
                'label' => 'UNC',
                'required' => false,
                'attr' => [
                    'min' => 0,
                ],
            ])
            ->add('graveur', TextType::class, [
                'label' => 'Graveur',
                'required' => false,
            ])
            ->add('estimation', TextType::class, [
                'label' => 'Extimation',
                'required' => false,
            ])
            ->add('rare', IntegerType::class, [
                'label' => 'Rareté (sur 10)',
                'required' => false,
                'attr' => [
                    'min' => 0,
                    'max' => 10,
                ],
            ])
            ->add('imageFile',FileType::class, [
                'label' => 'Image de la pièce',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Piece::class,
        ]);
    }
}

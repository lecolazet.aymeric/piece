<?php

namespace App\Entity;

use App\Repository\PieceRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Entity(repositoryClass=PieceRepository::class)
 * @Vich\Uploadable()
 */
class Piece
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pays;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tirage;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $BE;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $BU;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $UNC;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $graveur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $estimation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rare;

    /**
     * @Vich\UploadableField(mapping="piece_image", fileNameProperty="namePicture")
     * @var File|null
     */
    private $imageFile;

    /**
     * @return File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @param File|null $imageFile
     * @return Piece
     */
    public function setImageFile(?File $imageFile): Piece
    {
        $this->imageFile = $imageFile;
        return $this;
    }

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $namePicture;

    /**
     * @return mixed
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @param mixed $titre
     */
    public function setTitre($titre): void
    {
        $this->titre = $titre;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(?string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTirage(): ?int
    {
        return $this->tirage;
    }

    public function setTirage(?int $tirage): self
    {
        $this->tirage = $tirage;

        return $this;
    }

    public function getBE(): ?int
    {
        return $this->BE;
    }

    public function setBE(?int $BE): self
    {
        $this->BE = $BE;

        return $this;
    }

    public function getBU(): ?int
    {
        return $this->BU;
    }

    public function setBU(?int $BU): self
    {
        $this->BU = $BU;

        return $this;
    }

    public function getUNC(): ?int
    {
        return $this->UNC;
    }

    public function setUNC(?int $UNC): self
    {
        $this->UNC = $UNC;

        return $this;
    }

    public function getGraveur(): ?string
    {
        return $this->graveur;
    }

    public function setGraveur(?string $graveur): self
    {
        $this->graveur = $graveur;

        return $this;
    }

    public function getEstimation(): ?string
    {
        return $this->estimation;
    }

    public function setEstimation(?string $estimation): self
    {
        $this->estimation = $estimation;

        return $this;
    }

    public function getRare(): ?int
    {
        return $this->rare;
    }

    public function setRare(?int $rare): self
    {
        $this->rare = $rare;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNamePicture()
    {
        return $this->namePicture;
    }

    /**
     * @param mixed $namePicture
     */
    public function setNamePicture($namePicture): void
    {
        $this->namePicture = $namePicture;
    }

}
